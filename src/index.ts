import {GameStarter} from "./engine/GameStarter";
import {PrepareGameCommand} from "./game/commands/PrepareGameCommand";
import {EventEmitter} from "events";
import {Sequence} from "./engine/command/Command";
import {CenterContainerMediator} from "./game/mediators/CenterContainerMediator";
import {GameModel} from "./game/models/GameModel";
import {ShowGameSceneCommand} from "./game/commands/ShowGameSceneCommand";
import {ServerEmulatorModel, ServerModel} from "./game/models/ServerMode";

const gameStarter = new GameStarter();

gameStarter.start({
	render: {
		parentHTML: document.body,
		screenSizeRangeX: [16 * 32, 16 * 32],
		screenSizeRangeY: [16 * 18, 16 * 18],
		bgColor: 0x555555
	},
	di: [
		{classSource: EventEmitter, singleton: true, force: true},
		{classSource: ServerModel, classSourceMapping: ServerEmulatorModel, singleton: true, force: true},
		{classSource: GameModel, singleton: true, force: true},
	],
	mediatorsMap: {
		"centerContainer": [CenterContainerMediator],
	},
	fonts: [
		{name: 'Fredoka One', settings: {weight: 400}}
	],
	prepareAction: new PrepareGameCommand(),
	gameLoop: new Sequence(new ShowGameSceneCommand()),
});