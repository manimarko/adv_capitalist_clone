import {AnimatedView} from "../../engine/animation/AnimatedView";
import {Mediator} from "../../engine/Mediator";

export class CenterContainerMediator extends Mediator<any> {
	firstTime = true;

	init(view: AnimatedView): void {
		super.init(view);
		this.view.alpha = 0;
	}

	update(): void {
		if (this.firstTime) {
			this.view.alpha = 1;
			this.view.x = -this.view.width * 0.5;
			this.firstTime = false;
		} else {
			this.view.x += (-this.view.width * 0.5 - this.view.x) * 0.1;
		}
	}
}