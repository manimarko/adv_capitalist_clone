import {AnimatedView} from "../../engine/animation/AnimatedView";
import {Mediator} from "../../engine/Mediator";

export class ButtonMediator extends Mediator<AnimatedView> {

	init(view: AnimatedView): void {
		super.init(view);
		this.view.interactive = this.view.interactiveChildren = true;
		this.view.addListener("touchstart", this.onTap.bind(this));
		this.view.addListener("mousedown", this.onTap.bind(this));
		this.view.animator.setCurrentAnimation("idle", 0, 0.1);
	}

	onTap(e): void {
		if (this.view.buttonMode) {
			this.view.animator.tracks[0].currentAnimation.time = 0;
			this.view.sequencePlayer.playSequence([
				{name: "tap", trackIndex: 0, time: 0.2, blendTime: 0.0},
				{name: "idle", trackIndex: 0, time: 0.4, blendTime: 0.2}
			]);
		}
	}
}