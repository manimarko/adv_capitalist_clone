import {HSLUtils} from "../../engine/HSLUtils";
import {colors} from "../Constants";

const h = 23 / 360;
const s = 0.2;
const l = 0.8;

const bodyl = l + 0.05;
const bodys = s;


const color_brown = HSLUtils.hslToHex(0.15, 0.6, 0.8);
const color_eye = HSLUtils.hslToHex(0.15, 0.6, 0.8);
const color_mouth = HSLUtils.hslToHex(0.15, 0.6, 0.8);

const color_body = HSLUtils.hslToHex(h, bodys, bodyl);
const color_part = HSLUtils.hslToHex(h, s, l);

const color_body_front = HSLUtils.hslToHex(h, bodys, bodyl + 0.2);
const color_body_tail = HSLUtils.hslToHex(h, bodys + 0.1, bodyl + 0.1);
const color_body_tail_end = HSLUtils.hslToHex(h, bodys, 1);

const color_ear = HSLUtils.hslToHex(h, bodys + 0.1, bodyl - 0.4);

const W = 40;
const H = 54;
const R = 12;

const size = 10;

const hand_y = 35;
const face_y = 10;
const leg_distance = 8;
const ear_len = 20;

export const RabbitViewData = {
	structure: [
		{roundRect: {w: 40, h: 16, color: colors.blackColor, r: 10}, pivot: [20, 4], alpha: 0.1, path: "shadow"},
		{
			roundRect: {w: size, h: size, color: color_part, r: size * 0.5},
			pivot: [size * 0.5, size * 0.5],
			pos: [-leg_distance, 0],
			path: "body.l_leg",
		},
		{
			roundRect: {w: size, h: size, color: color_part, r: size * 0.5},
			pivot: [size * 0.5, size * 0.5],
			pos: [leg_distance, 0],
			path: "body.r_leg"
		},

		{
			roundRect: {w: size * 2, h: size, color: color_part, r: size * 0.5},
			pivot: [size, size * 0.5],
			pos: [-W * 0.5 + 5, -hand_y],
			path: "body.l_hand"
		},
		{
			roundRect: {w: size * 2, h: size, color: color_part, r: size * 0.5},
			pivot: [size, size * 0.5],
			pos: [W * 0.5 - 5, -hand_y],
			path: "body.r_hand"
		},


		{
			pos: [0, -H],
			path: "body.ears"
		},

		{
			roundRect: {w: ear_len * 0.5, h: ear_len, color: color_part, r: 6},
			pivot: [ear_len * 0.25, ear_len * 0.5],
			pos: [-7, 0],
			path: "body.ears.l_ear"
		},

		{
			roundRect: {w: ear_len * 0.5, h: ear_len, color: color_part, r: 6},
			pivot: [ear_len * 0.25, ear_len * 0.5],
			pos: [7, 2],
			path: "body.ears.r_ear"
		},


		{pos: [0, 0], path: "body"},
		{roundRect: {w: W, h: H, color: color_body, r: R}, pivot: [W * 0.5, H], pos: [0, 0], path: "body.main_body"},

		{
			pos: [0, -(H - face_y)],
			path: "body.face"
		},

		{
			circle: {color: color_ear, r: 1.6},
			pivot: [0, 0],
			pos: [-10, 5],
			path: "body.face.l_eye"
		},

		{
			circle: {color: color_ear, r: 1.6},
			pos: [10, 5],
			path: "body.face.r_eye"
		},

		{
			circle: {color: color_ear, r: 2.2},
			pos: [0, 10],
			path: "body.face.mouth"
		},
		{
			circle: {color: color_ear, r: 5},
			pos: [0, 12],
			alpha: 0.2,
			path: "body.face.mouth2"
		},
		{pos: [0, 0], pivot: [0, 0], path: "tail"},
		{circle: {color: color_body_tail, r: 6}, pivot: [0, 0], pos: [0, -10], path: "tail.main"},
		{pos: [0, -H * 1.3], path: "head"},


	],
	animations: {
		'start_jump': [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: 0.2, scale: [-3, -1.5], delta: 0},
			{path: ["body", "l_leg"], key: "y", period: 0.2, scale: [1.5, 0.75], delta: 0},
			{path: ["body", "r_leg"], key: "y", period: 0.2, scale: [1.5, 0.75], delta: 0},

			{path: ["body", "l_hand"], key: "y", period: 0.2, scale: 0.2, begin: 0, delta: -0.5},
			{path: ["body", "r_hand"], key: "y", period: 0.2, scale: 0.2, begin: 0, delta: -0.5},

			{path: ["body", "ears", "l_ear"], key: "y", period: 0.2, scale: 1, begin: 0, delta: -0.5},
			{path: ["body", "ears", "r_ear"], key: "y", period: 0.2, scale: 1, begin: 0, delta: -0.5},
		],
		'landing': [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: 0.2, scale: [-3, -1.5], delta: 0},
			{path: ["body", "l_leg"], key: "y", period: 0.2, scale: [1.5, 0.75], delta: 0},
			{path: ["body", "r_leg"], key: "y", period: 0.2, scale: [1.5, 0.75], delta: 0},

			{path: ["body", "l_hand"], key: "y", period: 0.2, scale: 0.2, begin: 0, delta: -0.5},
			{path: ["body", "r_hand"], key: "y", period: 0.2, scale: 0.2, begin: 0, delta: -0.5},

			{path: ["body", "ears", "l_ear"], key: "y", period: 0.2, scale: 1, begin: 0, delta: -0.5},
			{path: ["body", "ears", "r_ear"], key: "y", period: 0.2, scale: 1, begin: 0, delta: -0.5},
		],
		"idle": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: 0.4, scale: 0.5},
			{path: ["body", "l_leg"], key: "y", period: 0.4, scale: 0.5, begin: 0, delta: 1},
			{path: ["body", "r_leg"], key: "y", period: 0.4, scale: 0.5, begin: 0, delta: 1},

			{path: ["body", "l_hand"], key: "angle", period: 0.4, scale: 4, begin: 0, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: 0.4, scale: 4, begin: 0, delta: -0.5},
		],
		'show_face': [
			{path: ["body", "face"], key: "alpha", period: 0.2, scale: 0, begin: 1, delta: 0},
			{path: ["tail"], key: "alpha", period: 0.2, scale: 0, begin: 0, delta: 0},
		],
		'hide_face': [
			{path: ["body", "face"], key: "alpha", period: 0.2, scale: 0, begin: 0, delta: 0},
			{path: ["tail"], key: "alpha", period: 0.2, scale: 0, begin: 1, delta: 0},
		],
		'rotate': [
			{scaleTime: 0.1},
		]
	}
}