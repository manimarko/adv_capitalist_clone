import {colors} from "../Constants";
import {HSLUtils} from "../../engine/HSLUtils";

export const CoinsContianerViewData = {
	structure: [
		{
			circle: {r: 12, color: HSLUtils.addLightness(colors.coinColor, -0.1)},
			pivot: [0, 0],
			pos: [10, 3],
			path: "centerContainer.coinBg"
		},
		{
			circle: {r: 12, color: colors.coinColor},
			pivot: [0, 0],
			pos: [10, 0],
			path: "centerContainer.coin"
		},
		{
			text: {text: "5000", style: "big"},
			pivot: [0, 0],
			pos: [26, -18],
			path: "centerContainer.tf"
		},
	],
	animations: {
	}
};
