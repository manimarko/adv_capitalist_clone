import { colors } from "../Constants";

export const BackgroundGrass = {
	structure: [
		{
			rect: { w: 800, h: 1200, color: colors.bgColor },
			pivot: [400, 600],
			pos: [0, 0],
			path: "body"
		}
	],
	animations: {}
};
