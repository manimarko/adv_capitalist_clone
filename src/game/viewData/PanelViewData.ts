import {colors} from "../Constants";
import {BuyButtonViewData} from "./BuyButtonViewData";
import {StartProduceButtonViewData} from "./StartProduceButtonViewData";
import {createPanel} from "./createPanel";
import {UnlockButtonViewData} from "./UnlockButtonViewData";
import {HSLUtils} from "../../engine/HSLUtils";

export const basePeriod = 0.5;

export const PanelViewData = {
	structure: [
		{
			pivot: [0, 0],
			pos: [-150, -40],
			path: "body",
		},
		{
			pivot: [0, 0],
			pos: [0, 0],
			path: "body.bg",
		},
		{
			pivot: [0, 0],
			pos: [80, 12],
			path: "body.progress",
		},
		...createPanel("body.bg", 300, 80, colors.panelColor, 20),
		{
			roundRect: {w: 210, h: 20, color: HSLUtils.addLightness(colors.panelColor, -0.1), r: 20},
			pivot: [0, 0],
			pos: [0, 0],
			path: "body.progress.bg"
		},
		{
			roundRect: {w: 200, h: 20, color: colors.progressColor, r: 20},
			pivot: [0, 0],
			pos: [0, 0],
			path: "body.progress.bar"
		},
		{
			text: {text: "aaa", style: "small", color: colors.whiteColor},
			anchor: [0.5, 0.5],
			pos: [100, 10],
			path: "body.progress.produceAmount"
		},
		{
			pivot: [0, 0],
			pos: [180, 54],
			source: BuyButtonViewData,
			path: "body.buyUpgradeButton"
		},
		{
			pivot: [0, 0],
			pos: [40, 35],
			source: StartProduceButtonViewData,
			path: "body.icon.startButton"
		},
		{
			pivot: [0, 0],
			pos: [185, 20],
			source: UnlockButtonViewData,
			path: "body.unlockButton"
		}
	],
	animations: {
		"locked": [
			{scaleTime: 0.2},
			{path: ["body", "unlockButton"], key: "alpha", period: 0.1, scale: 0, begin: 1, delta: 0},
			{path: ["body", "buyUpgradeButton"], key: "alpha", period: 0.1, scale: 0, begin: 0, delta: 0},
			{path: ["body", "progress"], key: "alpha", period: 0.1, scale: 0, begin: 0, delta: 0},
		],
		"unlocked": [
			{scaleTime: 0.2},
			{path: ["body", "unlockButton"], key: "alpha", period: 0.2, scale: 0, begin: 0, delta: 0},
			{path: ["body", "buyUpgradeButton"], key: "alpha", period: 0.2, scale: 0, begin: 1, delta: 0},
			{path: ["body", "progress"], key: "alpha", period: 0.1, scale: 0, begin: 1, delta: 0},
		],
	}
};
