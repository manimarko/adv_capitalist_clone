import {HSLUtils} from "../../engine/HSLUtils";

const color_white = HSLUtils.hslToHex(1, 1, 1);
const color_yellow = HSLUtils.hslToHex(0.15, 0.6, 0.8);
const color_green = HSLUtils.hslToHex(0.3, 0.5, 0.6);

export const flowerData = {
	structure: [
		{pos: [0, 0], path: "body.stem.p1"},
		{roundRect: {w: 8, h: 50, color: color_green, r: 4}, pivot: [4, 5], path: "body.stem.p1.gr"},
		{pos: [0, 40], angle: 0, path: "body.stem.p1.p2"},
		{roundRect: {w: 8, h: 50, color: color_green, r: 4}, pivot: [4, 5], path: "body.stem.p1.p2.gr"},

		{pos: [0, 40], cache: true, path: "body.stem.p1.p2.flower"},
		{roundRect: {w: 40, h: 10, color: color_white, r: 5}, pivot: [20, 5], path: "body.stem.p1.p2.flower.p1"},
		{
			roundRect: {w: 40, h: 10, color: color_white, r: 5},
			pivot: [20, 5],
			angle: 90,
			path: "body.stem.p1.p2.flower.p2"
		},
		{
			roundRect: {w: 40, h: 10, color: color_white, r: 5},
			pivot: [20, 5],
			angle: 45,
			path: "body.stem.p1.p2.flower.p3"
		},
		{
			roundRect: {w: 40, h: 10, color: color_white, r: 5},
			pivot: [20, 5],
			angle: -45,
			path: "body.stem.p1.p2.flower.p4"
		},
		{pos: [0, 0], circle: {r: 5, color: color_yellow}, path: "body.stem.p1.p2.flower.center"},

	],
	animations: {
		"idle": [
			{path: ["body", "stem", "p1"], key: "angle", period: 1, scale: 10, delta: 0},
			{path: ["body", "stem", "p1", "p2"], key: "angle", period: 1, scale: 10, delta: 0},
			{path: ["body", "stem", "p1", "p2", "flower"], key: "angle", period: 2, scale: 10, delta: 0},
		],
		"idle2": [
			{path: ["body", "stem", "p1"], key: "angle", period: 1, scale: 50, delta: 0},
			{path: ["body", "stem", "p1", "p2"], key: "angle", period: 1, scale: 10, delta: 0},
			{path: ["body", "stem", "p1", "p2", "flower"], key: "angle", period: 2, scale: 10, delta: 0},
		]
	}
}