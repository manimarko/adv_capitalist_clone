import {colors} from "../Constants";
import {createPanel} from "./createPanel";
import {CoinsContianerViewData} from "./CoinsContianerViewData";

export const UnlockButtonViewData = {
	structure: [
		...createPanel("body.bg", 200, 60, colors.unlockBtnColor, 20),
		{
			text: {text: "Unlock for:", style: "small", align: "center"},
			anchor: [0.5, 0.5],
			pos: [100, 16],
			path: "body.unlock"
		},
		{
			source: CoinsContianerViewData,
			scale: [1, 1],
			pos: [100, 40],
			path: "body.coins"
		},
		{
			pivot: [100, 15],
			pos: [0, 0],
			path: "body"
		}
	],
	animations: {
		"idle": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0, begin: 1},
		],
		"tap": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0.2, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0.2, begin: 1},
		],
		"enabled": [
			{scaleTime: 0.5},
			{path: ["body"], key: "alpha", period: 0.15, scale: 0, begin: 1},
		],
		"disabled": [
			{scaleTime: 0.5},
			{path: ["body"], key: "alpha", period: 0.15, scale: 0, begin: 0.4},
		]
	}
};
