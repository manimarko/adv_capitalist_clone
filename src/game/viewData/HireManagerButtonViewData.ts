import {colors} from "../Constants";
import {createPanel} from "./createPanel";
import {CoinsContianerViewData} from "./CoinsContianerViewData";

export const HireManagerButtonViewData = {
	structure: [
		...createPanel("body.bg", 100, 70, colors.panelColor, 20),
		{
			source: CoinsContianerViewData,
			scale: [0.6, 0.6],
			pos: [50, 54],
			path: "body.coins"
		},
		{
			text: {text: "Hire\nManager", style: "small_description", align: "center"},
			anchor: [0.5, 0],
			pos: [50, 2],
			path: "body.description"
		},
		{
			pivot: [60, 35],
			pos: [0, 0],
			path: "body"
		}
	],
	animations: {
		"idle": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0, begin: 1},
		],
		"tap": [
			{scaleTime: 0.5},
			//{path: ["body"], key: "scaleX", period: 0.15, scale: 0.2, begin: 1},
			//{path: ["body"], key: "scaleY", period: 0.2, scale: 0.2, begin: 1},
		],
		"disabled": [
			{scaleTime: 0.5},
			{path: ["body"], key: "alpha", period: 0.15, scale: 0.0, begin: 0.5},
		],
		"enabled": [
			{scaleTime: 0.5},
			{path: ["body"], key: "alpha", period: 0.15, scale: 0.0, begin: 1}
		]
	}
};
