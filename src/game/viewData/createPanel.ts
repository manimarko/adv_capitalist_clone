import {HSLUtils} from "../../engine/HSLUtils";
import {colors} from "../Constants";

export const createPanel = (path, w, h, color, r) => {
	return [
		{
			roundRect: {w: w + 6, h: h + 6, color: colors.blackColor, r: r},
			pos: [-3, 3],
			alpha: 0.05,
			path: path + ".shadow",
		},
		{
			roundRect: {w: w, h: h, color: HSLUtils.addLightness(color, -0.1), r: r},
			pivot: [0, 0],
			pos: [0, 4],
			path: path + ".bgBack"
		},
		{
			roundRect: {w: w, h: h, color: color, r: r},
			pivot: [0, 0],
			pos: [0, 0],
			path: path + ".bg"
		},

	];
};

