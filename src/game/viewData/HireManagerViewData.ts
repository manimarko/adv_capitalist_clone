import {RabbitViewData} from "./RabbitViewData";
import {HireManagerButtonViewData} from "./HireManagerButtonViewData";

export const HireManagerViewData = {
	structure: [
		{
			source: HireManagerButtonViewData,
			scale: [1, 1],
			pos: [0, 0],
			path: "body.hireManagerButton"
		},
		{
			source: RabbitViewData,
			scale: [1, 1],
			pos: [0, 0],
			path: "body.manager"
		},
	],
	animations: {
		"disabled": [
			{path: ["body", "manager"], key: "alpha", period: 1, scale: 0.0, begin: 0},
			{path: ["body", "hireManagerButton"], key: "alpha", period: 1, scale: 0.0, begin: 1},
		],
		"enabled": [
			{path: ["body", "manager"], key: "alpha", period: 1, scale: 0.0, begin: 1},
			{path: ["body", "hireManagerButton"], key: "alpha", period: 1, scale: 0.0, begin: 0},
		]
	}
};
