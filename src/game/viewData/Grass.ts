import {colors} from "../Constants";
import {HSLUtils} from "../../engine/HSLUtils";

const basePeriod = 0.5;

export const Grass = {
	structure: [
		{
			circle: {r: 20, color: colors.blackColor},
			alpha: 0.1,
			pivot: [0, 0],
			pos: [0, 0],
			scale: [1, 0.5],
			path: "shadow"
		},
		{
			circle: {r: 20, color: colors.blackColor},
			alpha: 0.1,
			pivot: [0, 0],
			pos: [0, 0],
			scale: [0.5, 0.25],
			path: "shadow2"
		},
		{
			pivot: [0, 0],
			pos: [0, 0],
			path: "body",
		},
		{
			roundRect: {w: 24, h: 12, color: HSLUtils.addLightness(colors.grassColor,0.01), r: 4},
			angle: -65,
			pivot: [0, 5],
			pos: [-3, 0],
			path: "body.stem1"
		},
		{
			roundRect: {w: 20, h: 10, color: HSLUtils.addLightness(colors.grassColor,-0.01), r: 4},
			angle: -145,
			pivot: [0, 5],
			pos: [3, 0],
			path: "body.stem2"
		}
	],
	animations: {
		"idle": [
			{scaleTime: 0.1},
			{path: ["body", "stem1"], key: "angle", period: basePeriod * 0.5, scale: 5, delta: 0},
			{path: ["body", "stem2"], key: "angle", period: basePeriod * 0.5, scale: 5, delta: 0.5},
		]
	}
};
