
import { colors } from "../Constants";
import {createPanel} from "./createPanel";


export const StartProduceButtonViewData = {
	structure: [
		...createPanel("body.bg", 60, 60, colors.iconColor, 20),
		{
			pivot: [30, 30],
			pos: [0, 0],
			path: "body"
		},
		{
			image: "carrot",
			anchor: [0.5, 0.5],
			pos: [30, 30],
			path: "body.carrot",
			width: 60,
			angle: 45,
			height: 60
		},
	],
	animations: {
		"idle": [
			{ scaleTime: 0.5 },
			{ path: ["body"], key: "scaleX", period: 0.15, scale: 0, begin: 1 },
			{ path: ["body"], key: "scaleY", period: 0.2, scale: 0, begin: 1 },
		],
		"tap": [
			{ scaleTime: 0.5 },
			{ path: ["body"], key: "scaleX", period: 0.2, scale: 0.2, begin: 1 },
			{ path: ["body"], key: "scaleY", period: 0.15, scale: 0.2, begin: 1 },
		],
		"enabled": [
			{ path: ["body"], key: "alpha", period: 0.15, scale: 0, begin: 1 },
		],
		"disabled": [
			{ path: ["body"], key: "alpha", period: 0.15, scale: 0, begin: 0.4 },
		],
		"iconInProgress": [
			{path: ["body", "carrot"], key: "angle", period: 0.5, scale: 10, begin: 0, delta: 0},
		],
		"iconIdle": [
			{path: ["body", "carrot"], key: "angle", period: 0.5, scale: 0, begin: 0, delta: 0},
		]
	}
};
