import {colors} from "../Constants";
import {createPanel} from "./createPanel";
import {CoinsContianerViewData} from "./CoinsContianerViewData";

export const BuyButtonViewData = {
	structure: [
		...createPanel("body.bg", 120, 30, colors.buyBtnColor, 20),
		{
			source: CoinsContianerViewData,//,,text: {text: "5000", style: "small", align: "center"},
			//anchor: [0.5, 0.5],
			scale: [0.6, 0.6],
			pos: [60, 15],
			path: "body.coins"
		},

		{
			pivot: [60, 15],
			pos: [0, 0],
			path: "body"
		}
	],
	animations: {
		"idle": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0, begin: 1},
		],
		"tap": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0.2, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0.2, begin: 1},
		],
		"enabled": [
			{scaleTime: 0.5},
			{path: ["body"], key: "alpha", period: 0.15, scale: 0, begin: 1},
		],
		"disabled": [
			{scaleTime: 0.5},
			{path: ["body"], key: "alpha", period: 0.15, scale: 0, begin: 0.6},
		]
	}
};
