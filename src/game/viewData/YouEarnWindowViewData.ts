import {colors} from "../Constants";
import {createPanel} from "./createPanel";
import {CoinsContianerViewData} from "./CoinsContianerViewData";

export const CloseButtonViewData = {
	structure: [
		...createPanel("body.bg", 200, 60, colors.buyBtnColor, 20),
		{
			text: {text: "OK", style: "middle"},
			anchor: [0.5, 0.5],
			pos: [100, 30],
			path: "body.ok_tf"
		},
		{
			pivot: [100, 30],
			path: "body"
		},
	],
	animations: {
		"tap": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0.2, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0.2, begin: 1},
		],
		"idle": [
			{scaleTime: 0.5},
			{path: ["body"], key: "scaleX", period: 0.15, scale: 0, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.2, scale: 0, begin: 1},
		],
	}
};

export const YouEarnWindowViewData = {
	structure: [
		{
			rect: {w: 768, h: 1136, color: colors.blackColor},
			pivot: [768 * 0.5, 1136 * 0.5],
			alpha: 0.5,
			pos: [0, 0],
			interactive: true,
			path: "background"
		},
		...createPanel("body", 500, 300, colors.panelColor, 20),
		{
			pivot: [250, 150],
			pos: [0, 0],
			path: "body"
		},
		{
			source: CloseButtonViewData,
			pivot: [0, 0],
			pos: [250, 250],
			path: "body.closeButton"
		},
		{
			source: CoinsContianerViewData,
			pos: [250, 110],
			scale: [2, 2],
			path: "body.coinsCont"
		},
		{
			text: {text: "You  earned:", style: "big"},
			anchor: [0.5, 0.5],
			pos: [250, 50],
			path: "body.descr1"
		},
		{
			text: {text: "While you were gone.", style: "big"},
			anchor: [0.5, 0.5],
			pos: [250, 170],
			path: "body.descr2"
		},
	],
	animations: {
		"show": [
			{path: ["body"], key: "scaleX", period: 0.5, scale: 0.05, begin: 1},
			{path: ["body"], key: "scaleY", period: 0.5, scale: 0.05, begin: 1},
			{path: ["background"], key: "alpha", period: 0.1, scale: 0.0, begin: 0.5},
			{path: ["body"], key: "alpha", period: 0.1, scale: 0.0, begin: 1},
		],
		"hide": [
			{path: ["background"], key: "alpha", period: 0.1, scale: 0.0, begin: 0},
			{path: ["body"], key: "scaleXY", period: 0.1, scale: 0.0, begin: 0, delay: 0.2},
		],
	}
};
