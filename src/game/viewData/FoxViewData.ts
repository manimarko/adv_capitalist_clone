import {HSLUtils} from "../../engine/HSLUtils";
import {colors} from "../Constants";

const h = 23 / 360;
const s = 0.8;
const l = 0.53;

const bodyl = l + 0.05;
const bodys = s;

const color_brown = HSLUtils.hslToHex(0.15, 0.6, 0.8);
const color_eye = HSLUtils.hslToHex(0.15, 0.6, 0.8);
const color_mouth = HSLUtils.hslToHex(0.15, 0.6, 0.8);

const color_body = HSLUtils.hslToHex(h, bodys, bodyl);
const color_part = HSLUtils.hslToHex(h, s, l);

const color_body_front = HSLUtils.hslToHex(h, bodys, bodyl + 0.2);
const color_body_tail = HSLUtils.hslToHex(h, bodys + 0.1, bodyl + 0.1);
const color_body_tail_end = HSLUtils.hslToHex(h, bodys, 1);

const color_ear = HSLUtils.hslToHex(h, bodys + 0.1, bodyl - 0.4);

const W = 60;
const H = 80;
const R = 15;

const size = 17;

const hand_y = 45;
const face_y = 15;

const basePeriod = 0.2;
const roundRatio = 0.5;

export const foxViewData = {
	structure: [
		{roundRect: {w: 56, h: 16, color: colors.blackColor, r: 10}, pivot: [28, 4], alpha: 0.1, path: "shadow"},
		{
			roundRect: {w: size, h: size, color: color_part, r: size * roundRatio},
			pivot: [size * 0.5, size * 0.5],
			pos: [-15, 0],
			path: "l_leg",
		},
		{
			roundRect: {w: size, h: size, color: color_part, r: size * roundRatio},
			pivot: [size * 0.5, size * 0.5],
			pos: [15, 0],
			path: "r_leg",
		},

		{
			roundRect: {w: size * 2, h: size, color: color_part, r: size * roundRatio},
			pivot: [size, size * 0.5],
			pos: [-W * 0.5 + 5, -hand_y],
			path: "body.l_hand",
		},
		{
			roundRect: {w: size * 2, h: size, color: color_part, r: size * roundRatio},
			pivot: [size, size * 0.5],
			pos: [W * 0.5 - 5, -hand_y],
			path: "body.r_hand",
		},
		{
			//roundRect: {w: size * 2, h: size * 2, color: color_brown, r: 5},
			pivot: [0, 0],
			pos: [36, -26],
			angle: 15,
			path: "body.r_hand.flag",
		},

		{
			roundRect: {w: 4, h: 40, color: color_brown, r: 2},
			pivot: [0, 0],
			pos: [0, 0],
			path: "body.r_hand.flag.stem",
		},

		{
			roundRect: {w: 30, h: 21, color: colors.whiteColor, r: 2},
			pivot: [0, 0],
			pos: [0, 0],
			path: "body.r_hand.flag.plate",
		},

		{
			rect: {w: 30, h: 7, color: colors.redColor, r: 2},
			pivot: [0, 0],
			pos: [0, 7],
			path: "body.r_hand.flag.plate2",
		},

		{
			pos: [0, -H],
			path: "body.ears",
		},

		{
			roundRect: {w: 20, h: 20, color: color_ear, r: 10},
			pivot: [10, 10],
			pos: [-W * 0.5 + 15, 0],
			path: "body.ears.l_ear",
		},

		{
			roundRect: {w: 20, h: 20, color: color_ear, r: 10},
			pivot: [10, 10],
			pos: [W * 0.5 - 15, 0],
			path: "body.ears.r_ear",
		},


		{pos: [0, 0], path: "body",},
		{roundRect: {w: W, h: H, color: color_body, r: R}, pivot: [W * 0.5, H], pos: [0, 0], path: "body.main_body"},
		{
			roundRect: {w: W * 0.6, h: H * 0.4, color: color_body_front, r: R},
			pivot: [W * 0.3, H * 0.4],
			pos: [0, 0],
			path: "body.up",
		},

		{
			pos: [0, -(H - face_y)],
			path: "body.face",
		},

		{
			circle: {color: color_ear, r: 1.6},
			pivot: [0, 0],
			pos: [-10, 5],
			path: "body.face.l_eye",
		},

		{
			circle: {color: color_ear, r: 1.6},
			pos: [10, 5],
			path: "body.face.r_eye",
		},

		{
			roundRect: {w: 6, h: 3, color: color_ear, r: 1.5},
			pivot: [1, 1.5],
			angle: 180,//+15,
			pos: [-10, 0],
			path: "body.face.l_eye_brown",
		},

		{
			roundRect: {w: 6, h: 3, color: color_ear, r: 1.5},
			pivot: [1, 1.5],
			angle: 0,//;-15,
			pos: [10, 0],
			path: "body.face.r_eye_brown",
		},

		{
			circle: {color: color_ear, r: 2.2},
			pos: [0, 10],
			path: "body.face.mouth",
		},

		{pos: [0, 0], pivot: [0, 30], path: "tail"},
		{roundRect: {w: 30, h: 30, color: color_body_tail, r: R}, pivot: [15, 0], path: "tail.main"},
		{roundRect: {w: 20, h: 20, color: color_body_tail_end, r: 10}, pivot: [10, 0], pos: [0, 0], path: "tail.end"},
	],
	animations: {
		"run_up": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: basePeriod * 0.5, scale: 2, delta: 0.5},
			{path: ["body"], key: "angle", period: basePeriod, scale: 2},
			{path: ["l_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: 0.5},
			{path: ["r_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: -0.5},

			{path: ["l_leg"], key: "x", value: 0},
			{path: ["r_leg"], key: "x", value: 0},

			{path: ["body", "l_hand"], key: "angle", period: basePeriod, scale: 5, begin: 0, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: basePeriod, scale: 20, begin: 0, delta: -0.5},

			{path: ["tail"], key: "y", period: basePeriod * 0.5, scale: 1, begin: 0, delta: 0.5},
			{path: ["tail"], key: "angle", period: basePeriod, scale: 10, begin: 0, delta: -0.5},
			{path: ["tail"], key: "alpha", period: basePeriod, scale: 0, begin: 1, delta: 0},

			{path: ["body", "face"], key: "alpha", period: basePeriod, scale: 0, begin: 0, delta: 0},
			{path: ["body", "up"], key: "alpha", period: basePeriod, scale: 0, begin: 0, delta: 0},
			{path: ["body", "ears"], key: "y", period: basePeriod * 2, scale: 1, begin: 0, delta: 0.5}
		],
		"run_down": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: basePeriod * 0.5, scale: 2, delta: 0},
			{path: ["body"], key: "angle", period: basePeriod, scale: 2, delta: -0.5},
			{path: ["l_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: -0.5},
			{path: ["r_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: 0.5},

			{path: ["l_leg"], key: "x", value: 0},
			{path: ["r_leg"], key: "x", value: 0},

			{path: ["body", "l_hand"], key: "angle", period: basePeriod, scale: 5, begin: 0, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: basePeriod, scale: 20, begin: 0, delta: -0.5},

			{path: ["tail"], key: "y", period: basePeriod * 0.5, scale: 1, begin: 0, delta: -0.5},
			{path: ["tail"], key: "angle", period: basePeriod, scale: 10, begin: 0, delta: 0.5},
			{path: ["tail"], key: "alpha", period: basePeriod, scale: 0, begin: 0, delta: 0},


			{path: ["body", "face"], key: "alpha", period: basePeriod, scale: 0, begin: 1, delta: 0},
			{path: ["body", "up"], key: "alpha", period: basePeriod, scale: 0, begin: 1, delta: 0},
			{path: ["body", "ears"], key: "y", period: basePeriod * 2, scale: 1, begin: 0, delta: -0.5}
		],
		"run_left": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: basePeriod * 0.5, scale: 2, delta: 0},
			{path: ["body"], key: "angle", period: basePeriod, scale: 2, delta: 0},
			{path: ["l_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: 0.5},
			{path: ["r_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: -0.5},

			{path: ["l_leg"], key: "x", period: basePeriod, scale: 2, begin: 0, delta: -0.5},
			{path: ["r_leg"], key: "x", period: basePeriod, scale: 2, begin: 0, delta: -0.5},

			{path: ["body", "l_hand"], key: "angle", period: basePeriod, scale: 5, begin: 0, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: basePeriod, scale: 20, begin: 0, delta: -0.5},

			{path: ["tail"], key: "y", period: basePeriod * 0.5, scale: 1, begin: 0, delta: -0.5},
			{path: ["tail"], key: "angle", period: basePeriod, scale: 10, begin: 0, delta: 0.5},
			{path: ["body", "ears"], key: "y", period: basePeriod * 2, scale: 1, begin: 0, delta: -0.5},
		],
		"run_right": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: basePeriod * 0.5, scale: 2, delta: 0},
			{path: ["body"], key: "angle", period: basePeriod, scale: 2, delta: -0.5},
			{path: ["l_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: 0},
			{path: ["r_leg"], key: "y", period: basePeriod, scale: 2, begin: -5, delta: 0.5},

			{path: ["l_leg"], key: "x", period: basePeriod, scale: 2, begin: 0, delta: 0.5},
			{path: ["r_leg"], key: "x", period: basePeriod, scale: 2, begin: 0, delta: -0.5},

			{path: ["body", "l_hand"], key: "angle", period: basePeriod, scale: 5, begin: 0, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: basePeriod, scale: 20, begin: 0, delta: -0.5},

			{path: ["tail"], key: "y", period: basePeriod * 0.5, scale: 1, begin: 0, delta: -0.5},
			{path: ["tail"], key: "angle", period: basePeriod, scale: 10, begin: 0, delta: 0.5},
			{path: ["body", "ears"], key: "y", period: basePeriod * 2, scale: 1, begin: 0, delta: -0.5}
		],
		"idle": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: basePeriod, scale: 1},
			{path: ["body"], key: "angle", period: basePeriod, scale: 0, delta: 0, begin: 0},

			{path: ["l_leg"], key: "y", period: basePeriod * 2, scale: 0, begin: -3, delta: 0.5},
			{path: ["r_leg"], key: "y", period: basePeriod * 2, scale: 0, begin: -3, delta: -0.5},

			{path: ["l_leg"], key: "x", value: 0},
			{path: ["r_leg"], key: "x", value: 0},

			{path: ["tail"], key: "y", period: basePeriod * 2, scale: 1, begin: 0, delta: 0.5},
			{path: ["tail"], key: "angle", value: 0},

			{path: ["body", "l_hand"], key: "angle", period: basePeriod, scale: 2, begin: 0, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: basePeriod, scale: 2, begin: 0, delta: -0.5},

			{path: ["body", "l_hand"], key: "y", period: basePeriod, scale: 1, begin: 0, delta: -0.5},
			{path: ["body", "r_hand"], key: "y", period: basePeriod, scale: 1, begin: 0, delta: -0.5},

			{path: ["body", "ears"], key: "y", period: basePeriod, scale: 1, begin: 0, delta: 0.5}
		],

		"idle_trophy": [
			{scaleTime: 0.5},
			{path: ["body"], key: "y", period: basePeriod, scale: 1},
			{path: ["body"], key: "angle", period: basePeriod, scale: 0, delta: 0, begin: 0},

			{path: ["l_leg"], key: "y", period: basePeriod * 2, scale: 0, begin: -3, delta: 0.5},
			{path: ["r_leg"], key: "y", period: basePeriod * 2, scale: 0, begin: -3, delta: -0.5},

			{path: ["l_leg"], key: "x", value: 0},
			{path: ["r_leg"], key: "x", value: 0},

			{path: ["body", "l_hand"], key: "angle", period: basePeriod, scale: 2, begin: -20, delta: 0.5},
			{path: ["body", "r_hand"], key: "angle", period: basePeriod, scale: 2, begin: 20, delta: -0.5},

			{path: ["body", "l_hand"], key: "y", period: basePeriod, scale: 1, begin: 0, delta: -0.5},
			{path: ["body", "r_hand"], key: "y", period: basePeriod, scale: 1, begin: 0, delta: -0.5},

			{path: ["body", "ears"], key: "y", period: basePeriod, scale: 1, begin: 0, delta: 0.5}
		],

		"throw_up_left": [
			{path: ["body", "r_hand"], key: "scaleX", period: 1, scale: -0.9, begin: 1, delta: 0.0},
		],

		"throw_down_left": [
			{path: ["body", "r_hand"], key: "scaleX", period: 1, scale: -0.9, begin: 1, delta: 0.0},
		],

		"throw_up_right": [
			{path: ["body", "r_hand"], key: "scaleX", period: 1, scale: -0.9, begin: 1, delta: 0.0},
		],

		"throw_down_right": [
			{path: ["body", "r_hand"], key: "scaleX", period: 1, scale: -0.9, begin: 1, delta: 0.0},
		],

		"throw_idle": [
			{path: ["body", "r_hand"], key: "scaleX", period: 0.05, scale: 0, begin: 1, delta: 0},
		],

		"show_tail": [
			{path: ["tail"], key: "alpha", value: 1},

			{path: ["body", "face"], key: "alpha", value: 0},
			{path: ["body", "up"], key: "alpha", value: 0},
		],
		"show_face": [
			{path: ["tail"], key: "alpha", value: 0},

			{path: ["body", "face"], key: "alpha", value: 1},
			{path: ["body", "up"], key: "alpha", value: 1},
		]
	}
}