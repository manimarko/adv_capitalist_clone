import {ICommand} from "../../engine/command/Command";
import {ServiceLocator} from "../../engine/ServiceLocator";
import {GameModel} from "../models/GameModel";
import {ViewManager} from "../../engine/ViewManager";
import {DialogView} from "../scenes/DialogView";

export class RestoreGameCommand implements ICommand {

	async execute(data?: any): Promise<any> {
		return Promise.resolve();
	}
}