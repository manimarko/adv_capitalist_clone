import {ICommand} from "../../engine/command/Command";
import {ServiceLocator} from "../../engine/ServiceLocator";
import {GameModel} from "../models/GameModel";
import {ServerModel} from "../models/ServerMode";

export class PrepareGameCommand implements ICommand {

	async execute(data?: any): Promise<any> {
		const serverModel: ServerModel = ServiceLocator.inject(ServerModel);
		const gameModel: GameModel = ServiceLocator.inject(GameModel);
		await serverModel.init();
	}
}