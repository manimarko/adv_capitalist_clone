import {HSLUtils} from "../engine/HSLUtils";

const saturation = 0.4;
export const colors = {

	bgColor: HSLUtils.hslToHex(118 / 360, saturation, 0.4),
	grassColor: HSLUtils.hslToHex(118 / 360, saturation, 0.45),

	panelColor: HSLUtils.hslToHex(40 / 360, saturation, 0.5),
	unlockBtnColor: HSLUtils.hslToHex(118 / 360, saturation, 0.6),
	buyBtnColor: HSLUtils.hslToHex(118 / 360, saturation, 0.6),
	iconColor: HSLUtils.hslToHex(40 / 360, saturation, 0.6),
	progressColor: HSLUtils.hslToHex(118 / 360, saturation, 0.4),

	greenColor: HSLUtils.hslToHex(118 / 360, saturation, 0.8),
	grayColor: HSLUtils.hslToHex(118 / 360, saturation, 0.4),
	coinColor: HSLUtils.hslToHex(49 / 360, 0.7, 0.6),

	whiteColor: HSLUtils.hslToHex(79 / 360, saturation, 1),
	redColor: HSLUtils.hslToHex(12 / 360, 1, 0.5),
	blackColor: HSLUtils.hslToHex(79 / 360, saturation, 0.0)
}

export const textStyles = {
	"small_description": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 18,
		wordWrap: true,
		lineHeight: 15,
		align: "center"
	}),

	"small": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 18
	}),

	"middle": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 24
	}),

	"big": new PIXI.TextStyle({
		fontFamily: "Fredoka One",
		fontWeight: "normal",
		fill: "white",
		fontSize: 30
	}),

}