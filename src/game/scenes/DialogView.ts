import {AnimatedView} from "../../engine/animation/AnimatedView";
import {IBaseView} from "../../engine/ViewManager";
import {YouEarnWindowViewData} from "../viewData/YouEarnWindowViewData";

export class DialogView extends PIXI.Container implements IBaseView {
	showData: any;
	priority: number = 1;

	onAdded(): void {
		const v = new AnimatedView(YouEarnWindowViewData, this.showData);
		v.name = "youEarnWindow";
		this.addChild(v);
	}

	onRemoved(): void {

	}

	update(dt: number): void {
	}

	resize(w: number, h: number): void {
		this.x = w * 0.5;
		this.y = h * 0.5;
	}

}