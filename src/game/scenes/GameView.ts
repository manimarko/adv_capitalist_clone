import {ServiceLocator} from '../../engine/ServiceLocator';
import {IBaseView} from "../../engine/ViewManager";
import {GameModel} from "../models/GameModel";
import {WhiteBoxBehaviour} from "../models/behaviours/WhiteBoxBehaviour";
import {PlayerBoxBehaviour} from "../models/behaviours/PlayerBoxBehaviour";
import {BlueBoxBehaviour} from "../models/behaviours/BlueBoxBehaviour";

export class GameView extends PIXI.Container implements IBaseView {
	showData: any;
	priority: number = 0;
	gameModel: GameModel = ServiceLocator.inject(GameModel);
	gr: PIXI.Graphics = new PIXI.Graphics();
	cont = new PIXI.Container();
	contEntities = new PIXI.Container();
	static GRID_SIZE = 16;
	static SCREEN_SIZE = {x: 32, y: 18};
	xIndex = 0;
	yIndex = 0;

	draw(count_x: number, count_y: number): void {
		this.gr.beginFill(0xFF6633, 1);
		this.gr.drawRect(0, 0, count_x * GameView.GRID_SIZE, count_y * GameView.GRID_SIZE);
		this.gr.endFill();
		this.gr.lineStyle(1, 0xFFFFFF);
		for (let i = 0; i < count_y; i++) {
			this.gr.moveTo(GameView.GRID_SIZE * i, 0);
			this.gr.lineTo(GameView.GRID_SIZE * i, GameView.GRID_SIZE * count_x);
		}
		for (let i = 0; i < count_x; i++) {
			this.gr.moveTo(0, GameView.GRID_SIZE * i);
			this.gr.lineTo(GameView.GRID_SIZE * count_y, GameView.GRID_SIZE * i);
		}
		this.gr.lineStyle(5, 0xFF0000);
		this.gr.drawRect(0, 0, GameView.GRID_SIZE * count_x, GameView.GRID_SIZE * count_y);
	}

	onAdded(): void {
		this.gr.clear();
		this.addChild(this.cont);
		this.cont.addChild(this.gr);
		this.cont.addChild(this.contEntities);
		this.draw(64, 64);
		this.cont.x = 0;
		this.cont.y = 0;

		this.gr.on('click', this.onClick.bind(this));
		this.gr.on('tap', this.onClick.bind(this));
		this.gr.interactive = this.gr.interactiveChildren = true;

		this.gameModel.addBehaviours([WhiteBoxBehaviour, PlayerBoxBehaviour, BlueBoxBehaviour]);
		for (let i = 0; i < GameModel.SIZE.x; i += 4) {
			for (let j = 0; j < GameModel.SIZE.y; j += 4) {
				this.gameModel.addEntity(WhiteBoxBehaviour.ID, {
					pos: {
						x: i,
						y: j
					}
				});
			}
		}
		for (let i = 0; i < 50; i++) {
			this.gameModel.addEntity(BlueBoxBehaviour.ID, {
				pos: {
					x: Math.floor(Math.random() * 64),
					y: Math.floor(Math.random() * 64)
				}
			});
		}
		this.gameModel.addEntity(PlayerBoxBehaviour.ID, {pos: {x: 32, y: 32}});
		this.gameModel.createGrid();
	}

	onClick(eventData) {
		//console.log(eventData);
		let x = Math.floor(eventData.data.global.x / GameView.GRID_SIZE) + this.xIndex;
		let y = Math.floor(eventData.data.global.y / GameView.GRID_SIZE) + this.yIndex;
		let player = this.gameModel.getEntities(PlayerBoxBehaviour.ID)[0];
		player.data.path = player.world.aStarInstance.findPath(player.data.pos, {x: x, y: y});

		//console.log(x, y);
	}

	onRemoved(): void {

	}

	update(dt: number): void {
		this.contEntities.removeChildren();
		this.gameModel.update(dt);
		this.gameModel.entities.forEach(entity => {
			entity.updateView(dt);
			this.contEntities.addChild(entity.view);
		});

		let player = this.gameModel.getEntities(PlayerBoxBehaviour.ID)[0];
		this.xIndex = player.data.pos.x - GameView.SCREEN_SIZE.x * 0.5;
		this.yIndex = player.data.pos.y - GameView.SCREEN_SIZE.y * 0.5;

		this.xIndex = Math.min(Math.max(0, this.xIndex), GameModel.SIZE.x - GameView.SCREEN_SIZE.x);
		this.yIndex = Math.min(Math.max(0, this.yIndex), GameModel.SIZE.y - GameView.SCREEN_SIZE.y);

		this.x += (-this.xIndex * GameView.GRID_SIZE - this.x) * 0.2;
		this.y += (-this.yIndex * GameView.GRID_SIZE - this.y) * 0.2;
	}

	resize(w: number, h: number): void {
		//this.x = 0;
		//this.y = 0;
	}

}