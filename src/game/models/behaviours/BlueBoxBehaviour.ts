import {IEntityBehaviour} from "../Entity";
import {GameModel} from "../GameModel";
import {drawView} from "./WhiteBoxBehaviour";

export const BlueBoxBehaviour: IEntityBehaviour = {
	getView: drawView,

	ID: "BlueBoxBehaviour",

	activeVisitors: [],

	init(data, world: GameModel) {

	},

	updateView(data, view: PIXI.Graphics): void {
		view.position.set(data.pos.x * 16, data.pos.y * 16);
	},

	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0x3366FF,
		};
	}
};