import {IEntityBehaviour} from "../Entity";
import {GameModel} from "../GameModel";

export const WhiteBoxBehaviour: IEntityBehaviour = {
	getView: drawView,

	ID: "WhiteBoxBehaviour",

	activeVisitors: [],

	init(data, world: GameModel) {

	},

	updateView(data, view: PIXI.Graphics): void {
		view.position.set(data.pos.x * 16, data.pos.y * 16);
	},

	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0xFFFFFF,
		};
	}
};

export const checkMiss = (data, dt, world: GameModel) => {

}

export function drawView(data): PIXI.Container {
	const view: PIXI.Graphics = new PIXI.Graphics();
	view.beginFill(data.color, 1);
	view.drawRect(0, 0, 16, 16);
	view.endFill();
	view.interactive = false;
	view.interactiveChildren = false;
	return view;
};