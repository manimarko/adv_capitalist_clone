import {IEntityBehaviour} from "../Entity";
import {GameModel} from "../GameModel";
import {drawView} from "./WhiteBoxBehaviour";

export const YellowBoxBehaviour: IEntityBehaviour = {
	getView: drawView,

	ID: "YellowBoxBehaviour",

	activeVisitors: [mineBehaviour],

	init(data, world: GameModel) {

	},

	updateView(data, view: PIXI.Graphics): void {
		view.position.set(data.pos.x * 16, data.pos.y * 16);
	},

	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0x3366FF,
		};
	}
};

export function mineBehaviour(data: any, dt: number, world: GameModel) {

}
