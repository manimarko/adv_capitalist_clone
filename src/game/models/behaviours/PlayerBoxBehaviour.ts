import {IEntityBehaviour} from "../Entity";
import {GameModel} from "../GameModel";
import {drawView} from "./WhiteBoxBehaviour";
import {YellowBoxBehaviour} from "./YellowBoxBehaviour";

export const updateAnimations = (data: any, dt: number, world: GameModel) => {

}

export const moveToPoint = (data: any, dt: number, world: GameModel) => {
	if (data.path && data.path.length > 0 && data.time > 0.1) {
		let obj = data.path.shift();
		data.pos.x = obj[0];
		data.pos.y = obj[1];
		data.time = 0;
	}
	data.time += dt;
}

export const createMine = (data: any, dt: number, world: GameModel) => {
	//world.createStack.push({id: YellowBoxBehaviour.ID, pos: {...data.pos}});
}

export const PlayerBoxBehaviour: IEntityBehaviour = {
	getView: drawView,

	ID: "PlayerBoxBehaviour",

	activeVisitors: [updateAnimations, moveToPoint, createMine],

	init(data: any, world: GameModel) {

	},

	updateView(data, view: PIXI.Graphics): void {
		view.position.set(data.pos.x * 16, data.pos.y * 16);
	},

	getInitialData(): any {
		return {
			pos: {x: 0, y: 0},
			color: 0x66FF33,
			time: 0
		};
	}
};