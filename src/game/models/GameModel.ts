import {Entity, IEntityBehaviour} from "./Entity";
import {AStarFinder} from "astar-typescript";
import {BlueBoxBehaviour} from "./behaviours/BlueBoxBehaviour";

export class GameModel {
	static SIZE = {x: 64, y: 64};
	entities: Entity[] = [];
	createStack = [];
	behaviours: { [key: string]: IEntityBehaviour } = {};
	entityIndex = 0;
	aStarInstance: AStarFinder;

	constructor() {

	}

	createGrid(): void {
		let grid = [];
		for (let i = 0; i < GameModel.SIZE.y; i++) {
			grid.push(new Array(GameModel.SIZE.x));
		}

		let entities = this.getEntities(BlueBoxBehaviour.ID);
		entities.forEach(entity => {
			grid[entity.data.pos.y][entity.data.pos.x] = 1;
		});

		this.aStarInstance = new AStarFinder({
			grid: {
				matrix: grid
			},
			diagonalAllowed: false,
		});
	}

	reset() {
		this.entities = [];
	}

	get isCompleted(): boolean {
		return false;
	}

	addEntity(behaviorID: string, data: any): void {
		const behavior = this.behaviours[behaviorID];
		const entity = new Entity();
		entity.behaviour = behavior;
		entity.data = {...behavior.getInitialData(), ...data};
		entity.data.id = behaviorID;
		entity.data.internalId = this.entityIndex.toString();
		entity.world = this;
		entity.init();
		this.entities.push(entity);
		this.entityIndex++;
	}

	addBehaviours(arr: IEntityBehaviour[]) {
		arr.forEach((item, i) => {
			this.behaviours[item.ID] = item;
		});
	}

	update(dt: number): void {
		this.entities.forEach(entity => entity.update(dt));
		this.entities = this.entities.filter(entity => !entity.data.mustRemove);
		this.createStack.forEach(item => {
			this.addEntity(item.behaviourId, item.data);
		});
		this.createStack = [];
	}

	getGrid(): number[][] {
		return [];
	}

	getEntities(id: string): Entity[] {
		return this.entities.filter(entity => entity.data.id === id);
	}

	findPath(startPos: { x: number, y: number }, endPos: { x: number, y: number }): number[][] {
		return this.aStarInstance.findPath(startPos, endPos);
	}
}