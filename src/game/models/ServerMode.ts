export class ServerModel {
	localStorageKey = "gameStorage";
	storage: IGameData;
	staticData: { [key: string]: IGeneratorStaticData } = {};

	async init(): Promise<any> {

	}

	async save(data: any): Promise<void> {

	}

	get state(): IGameData {
		return null;
	}
}

export class ServerEmulatorModel extends ServerModel {
	localStorageKey = "gaaa3nns892j2jsh67723hss";
	storage: IGameData;
	staticData: { [key: string]: IGeneratorStaticData } = {};

	async init(): Promise<void> {

		if (!localStorage.getItem(this.localStorageKey)) {
			this.save(this.defaultGeneratorsData);
		}

		this.storage = JSON.parse(localStorage.getItem(this.localStorageKey));
	}

	get defaultGeneratorsData(): IGameData {
		return {
			money: 200,
			generators: <any>Object.values(this.staticData).map(item => {
				return {
					id: item.id,
					startProductionTime: 0,
					generatedMoney: 0,
					stageIndex: 0,
					autoGenerate: false,
					state: GeneratorStates.IS_IDLE,
					isLocked: true
				}
			})
		};
	}

	async save(data: any): Promise<void> {
		localStorage.setItem(this.localStorageKey, JSON.stringify(data));
	}

	get state(): IGameData {
		return this.storage;
	}
}

export interface IGameData {
	generators: IGeneratorData[],
	money: number;
};

export enum GeneratorStates {
	IS_IDLE,
	IN_PROGRESS,
	COMPLETED,
}

export interface IGeneratorStaticData {
	id: string,
	cost: number,
	ratio: number,
	productionTime: number,
	productionAmount: number,
	unlockCost: number,
	autoGenerate: {
		maxGenerationsCount: number,
		cost: number
	}
};

export interface IGeneratorData {
	id: string,
	startProductionTime: number,
	generatedMoney: number,
	stageIndex: 0,
	autoGenerate: boolean,
	state: GeneratorStates,
	isLocked: boolean
};