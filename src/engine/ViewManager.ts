import {Render} from "./Render";

export const SceneEvents = {
	INIT: "SceneManager.Init",
	ADD_SCENE: "SceneManager.Add",
	REMOVE_SCENE: "SceneManager.Remove"
}

export class ViewManager {
	static sceneRoot: PIXI.Container = new PIXI.Container();


	static show(sceneClass: any, data: any = null): void {
		if (this.find(sceneClass)) {
			return;
		}
		const view: IBaseView & PIXI.Container = new sceneClass();
		view.showData = data;
		this.sceneRoot.addChild(view);
		this.sort();
		view.onAdded();
		view.resize(Render.screenWidth, Render.screenHeight);
	}

	static sort(): void {
		this.sceneRoot.children.sort((a: any, b: any) => a.priority - b.priority);
	}

	static time = 0;

	static update(dt: number): void {
		const fixed_dt = 0.016;
		this.time += dt;
		let count = 0;
		while (this.time > 0) {
			this.sceneRoot.children.forEach((child: any) => {
				child.update(fixed_dt);
			});
			this.time -= fixed_dt;
			count++;
		}
	}

	static resize(w: number, h: number): void {
		this.sceneRoot.children.forEach((child: any) => {
			child.resize(w, h);
		});
	}

	static find(sceneClass: any): any {
		return this.sceneRoot.children.find(child => child instanceof sceneClass);
	}

	static hide(sceneClass: any): void {
		const obj: any = this.find(sceneClass);
		if (obj) {
			this.sceneRoot.removeChild(obj);
			obj.onRemoved();
		}
	}

	static hideAllExcept(sceneClasses: any[]): void {
		this.sceneRoot.children.concat().forEach(child => {
			if (!sceneClasses.some(sceneClass => child instanceof sceneClass)) {
				ViewManager.hide(child.constructor);
			}
		});
		sceneClasses.forEach(sceneClass => this.show(sceneClass))
	}
}

export interface IBaseView {
	showData: any;

	update(dt: number);

	resize(w: number, h: number);

	onAdded();

	onRemoved();

	priority: number;
}