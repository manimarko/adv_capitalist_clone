export interface ICommand {
	execute(data?: any): Promise<any>;
}

const generateMap = (arr) => {
	return arr.map(obj => {
		if (obj.prototype) {
			return new obj();
		}
		return obj;
	});
}

export class Sequence implements ICommand {
	children: ICommand[] = [];

	constructor(...children) {
		this.children = generateMap(children);
	}

	async execute(): Promise<any> {
		for (let i = 0; i < this.children.length; i++) {
			console.log(this.children[i])
			await this.children[i].execute();
		}
	}
}

export class Parallel implements ICommand {
	children: ICommand[] = [];

	constructor(...children) {
		this.children = generateMap(children);
	}

	async execute(): Promise<any> {
		const promises = this.children.map(command => command.execute());
		await Promise.all(promises);
	}
}

export class Selector implements ICommand {
	selectorFunction: Function;

	constructor(selectorFunction) {
		this.selectorFunction = selectorFunction;
	}

	async execute(): Promise<any> {
		let command = this.selectorFunction();
		if (command.prototype) {
			command = new command();
		}
		console.log(command);
		return await command.execute();
	}

}

const eventsMap = {};

export const bindCommand = (obj, eventName: string) => {
	if (!eventsMap[eventName]) {
		eventsMap[eventName] = [];
	}
	eventsMap[eventName].push(obj);
}

export const executeCommand = async (name, data = null) => {
	const commands: any[] = eventsMap[name];
	const promises = [];

	commands.forEach(commandConstructor => {
		const newCommand: ICommand = new commandConstructor();
		promises.push(newCommand.execute(data));
	});

	return Promise.all(promises);
}