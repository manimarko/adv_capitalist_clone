export class ServiceLocator {
	static map = new Map();

	static inject(obj) {
		const info = this.map.get(obj);

		if (info) {
			const clazz = info.classSourceMapping ? info.classSourceMapping : info.classSource;
			if (info.singleton) {
				if (!info.instance) {
					info.instance = new clazz();
				}
				return info.instance;
			}
			return new clazz();
		}
		return null;
	}

	static bindTo(data: any = {}) {
		const defaultData = {singleton: false, instance: null, force: false};
		const info = {...defaultData, ...data};
		this.map.set(data.classSource, info);
		if (info.force) {
			this.inject(data.classSource);
		}
	}

}