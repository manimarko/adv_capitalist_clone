import {Render} from "./Render";

export class Mediator<T extends PIXI.DisplayObject> {
	view: T;
	mapData:any;

	init(view: T): void {
		view.addListener("removed", this.onRemoved);
		Render.addTicker(this.update, this);
		this.view = view;
	}

	onRemoved(parent: PIXI.DisplayObject) {
		Render.removeTicker(this.update);
	}

	update(dt: number): void {
	}
}
