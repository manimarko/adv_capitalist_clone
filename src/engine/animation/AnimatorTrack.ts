import {Animator} from './Animator';

export class AnimatorTrack {
	currentAnimation: Animator;
	nextAnimation: Animator;
	values: any = {};
	blendTime: number = 0;
	blendKoef: number = 1;
	blendForce: number = 1;
	maxBlendTime: number = 1;

	setCurrentAnimation(nextAnimation: Animator): void {
		if ((nextAnimation !== this.currentAnimation && this.nextAnimation === null) || (nextAnimation !== this.currentAnimation && this.nextAnimation !== nextAnimation)) {
			this.nextAnimation = nextAnimation;
			this.nextAnimation.time = 0;
			this.blendTime = 0;
		}
	}

	updateCurrentAnimation(animation: Animator, ratio: number): void {
		if (animation) {
			Object.keys(animation.resultValues).forEach(key => {
				const value = animation.resultValues[key];
				if (ratio !== 1) {
					const prevValue = this.values.hasOwnProperty(key) ? this.values[key] : 0;
					this.values[key] = (value - prevValue) * ratio + prevValue;
				} else {
					this.values[key] = value;
				}
			});
		}
	}

	update(dt: number): void {
		this.blendTime += dt;
		if (this.currentAnimation) {
			this.currentAnimation.update(dt);
		}
		if (this.nextAnimation) {
			this.nextAnimation.update(dt);
		}

		this.blendKoef = this.maxBlendTime === 0 ? 1 : Math.pow(Math.min(this.blendTime / this.maxBlendTime, 1), this.blendForce);

		this.values = {};
		if (this.nextAnimation) {
			this.updateCurrentAnimation(this.currentAnimation, 1);
			this.updateCurrentAnimation(this.nextAnimation, this.blendKoef);
		} else {
			this.updateCurrentAnimation(this.currentAnimation, 1);
		}

		if (this.blendKoef === 1 && this.nextAnimation) {
			this.currentAnimation = this.nextAnimation;
			this.nextAnimation = null;
		}
	}
}
