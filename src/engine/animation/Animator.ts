export class Animator {
	animations: Array<IAnimationData> = [];
	time = 0;
	scale = 1;
	resultValues = {};

	addAnimations(arr) {
		const defaultData = {key: "", begin: 0, delta: 0, scale: 1};
		arr.forEach(item => {
			if (item.hasOwnProperty("scaleTime")) {
				this.scale = item.scaleTime;
				return;
			}
			const uniqKey = item.path.join(".") + ":" + item.key;
			this.animations.push({...defaultData, ...item, uniq_key: uniqKey});
		});
	}

	update(dt: number): void {
		this.time += dt * this.scale;
		this.animations.forEach(animation => {
			this.updateAnimation(animation);
		});
	}

	protected updateAnimation(animation: IAnimationData): void {
		let curr_time = this.time - (animation.delay ? animation.delay : 0);
		if (animation.period) {
			const periodicFunction = animation.periodicFunction ? animation.periodicFunction : Math.sin;
			if (animation.endTime && curr_time > animation.endTime * animation.period) {
				curr_time = animation.endTime * animation.period;
			}
			let k = (curr_time % animation.period) / animation.period;

			if (k < 0) {
				k = 0;
			}

			if (k >= 0) {
				const sinValue = periodicFunction(k * Math.PI * 2 + animation.delta * Math.PI);
				let newValue = 0;
				if (animation.scale instanceof Array) {
					const arr = animation.scale;
					if (sinValue > 0) {
						newValue = sinValue * arr[0];
					} else {
						newValue = sinValue * arr[1];
					}
				} else {
					newValue = sinValue * animation.scale;
				}

				this.resultValues[animation.uniq_key] = animation.begin + newValue;
			}
		} else {
			this.resultValues[animation.uniq_key] = animation.begin + animation.value;
		}
	}
}

export interface IAnimationData {
	key: string;
	period: number;
	begin: number;
	delta: number;
	scale: number | number[];
	value: number;
	uniq_key: string;
	path: string[];
	delay: number;
	abs: boolean;
	endTime: number;
	periodicFunction?: Function;
}